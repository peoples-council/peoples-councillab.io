+++
title = 'Peoples Health Science Institute'
date = 2024-04-26T23:25:41+05:30
draft = false
+++

Peoples Council of Education (PCE) proposes to set up Peoples
Health Science Institute with a view to discovering, developing and
disseminating a new democratic theory and praxis of health science so as
to enable the people of India to enjoy higher quality of material, social
and cultural life while living in harmony with Nature and Society.

It is proposed to locate the Peoples Health Science Institute in most
backward area where there is no modern facilities for medical science
education, health care and health education and training. The likely
location is Dudhi in Sonebhadra District of Uttar Pradesh or Vinobaji
Puram in Karur District of Tamil Nadu or Ambikapur in Chhatisgarh.
Dudhi is a most backward area in Sonebhadra District of Uttar Pradesh.
Location of proposed Peoples Health Science Institute at Dudhi having
networks of Village Health Centres in a radius of 150-200 Kilometers
will prove a boon to suffering masses of Sonebhadra and Mirzapur and
part of backward regions of Allahabad and Varanasi Districts in U.P.,
Rewa and Sahdol Districts of Madhya Pradesh, Ambikapur and Korea
Districts of Chhatisgarh and Garhwa and Palamau Districts of Jharkhand.

{{< figure src="/images/hospital0.png" class="img-fluid" >}}

It will open new opportunities of growth to the peoples of these regions.
Majority of the peoples of these areas are scheduled tribes and scheduled
castes who have been plundered even after India’s independence. Karur
District of Tamil Nadu too is equally backward and poor region. The
Peoples Health Science Institute would need 500 acres of land.

## CONCEPT

---

The concept of health involves capacity to enjoy life through
creative and fruitful productive works. This is possible only when
nutritious food, safe drinking water, clean clothes, hygienic living and
environmental conditions, good education, appropriate opportunity for
growth and education and easy and quick access to modern medical
facilities are available to every member of the given society. So health is
not merely absence of disease or illness. It is a much more broader
concept. Hitherto prevalent theory and praxis of health care is based on a
very narrow concept. Modern Medical Science places greater faith on
‘prevention’ rather than cure.
Therefore, it is necessary to build science of peoples health on the
broader concept of health.

The proposed Peoples Health Science Institute
would be rooted in the broader concept of health.
The contemporary health care is market-determined. It is the market
which sets all rules and policies of health care including medical science
education and drugs. ‘Maximisation of profit’ is the foundation of
market. Under such conditions only those can have access to modern
medical science education, medicine and access to hospitals who have
capacity to pay. Since majority of our people are poor they have no
access to it. Health care today means ruin of lower income groups. What
it means is modern health education and health care system is anti-
peoples and pro-rich. What is anti-people is undemocratic.

{{< figure src="/images/hospital4.png" class="img-fluid" >}}

It is, therefore, necessary to effect radical change in modern health science
education and health care by making it a truly democratic reflecting the
cardinal principles of equality, freedom and fraternity. The proposed
Peoples Health Science Institute, therefore, would be founded on the
theory and praxis of peoples’ democracy. In a sense, it will be an
altogether a novel national institute in the field of health science. It
involves radical change in the whole personality and behaviour of
medical/health practitioners and philosophy of health science.
A great deal of preparatory work is called for.

{{< figure src="/images/hospital5.png" class="img-fluid" >}}
Peoples Council of Education proposes to organize a series of local/regional/national workshops/
seminars/symposia on **‘Health Science Education’** with a view to
evolving the new concept and new policy. Organization of All India
Peoples Medical and Health Science Convention is designed to this end.

## STRUCTURE

---

Basically proposed Peoples Health Science Institute would be an
educational and research & training national institute in the field of
Health Science. It will be solely devoted to the task of discovering,
developing and disseminating new theory and praxis of health. Its whole
structure will be based on the principles of democracy and peoples’
sovereignty. Following units shall form its basic structure :

1. Peoples Hospital/Health Centre
2. Peoples Village Health Centres
   Peoples Council of Education/19
3. Peoples Nursing College
4. Peoples Undergraduate and Postgraduate Health Science Education
   And Research Centre/College
5. Peoples Health Communication And Training Centre
6. Peoples Ayurvedic & Herbal Plants/Medicine
7. Peoples Pharmaceutical Centre
   Peoples Hospital/Health Centre will be most advanced and referral
   centre with modern facilities for treatments of all kins of diseases and
   health care. It will be 3000 bedded hospital. It will be developed in 10
   phases.
8. First phase : 150 bedded
9. Second phase : 300 bedded
10. Third phase : 600 bedded
11. Fourth phase : 1000 bedded
12. Fifth phase : 1500 bedded
    6 Sixth phase : 2000 bedded
13. Seven phase : 2200 bedded
14. Eighth phase : 2500 bedded
15. Ninth phase : 2700 bedded
    10 Tenth phase : 3000 bedded
    manner :

All the necessary infrastructure will be built in phased manner in
period of 10-15 years. Peoples of all social classes shall have equal
access to the hospital.

{{< figure src="/images/hospital1.png" class="img-fluid" >}}

There will be Village Health Centre caring to the health needs of
5000 to 10,000 population. All the village health centres shall be integral
parts of the Peoples Hospital/Health Centre. All the necessary facilities
for health care will be available to the village peoples in their villages. In
the event of serious illness or tragedy the concerned person shall be taken
to central hospital – Peoples Health/Hospital Centre for appropriate
treatment and care.

A Peoples Nursing College will be set up for providing training to
paramedical personnel in democratic system of health education, care
and treatment. Pharmaceutical Education & Training Centre will train
young women and men in pharmacy.

Peoples Health Science Institute shall offer undergraduate and
postgraduate democratic education in the field of medical science and
health science. It will also create all the necessary conditions for research
and development in frontier areas.

The Institute shall also undertake production of Ayurvedic, Siddha,
Unani and herbal medicinal plants and drugs. Medicinal plants and herbs
will be grown on experimental farm of 200 acres through Peoples
Cooperatives to be set up in villages. It will propogate, produce and use
low cost drugs.

A Peoples Health Science Communication Centre will be developed
for communication of Health Science to the peoples.

## MECHANISM FOR HEALTH CARE TO ALL

---

It is almost impossible to ensure health care to all in the existing
market-determined society. However, a way out has to be worked out.
Involvement of peoples of all groups on democratic principles in the task
of building and management of the proposed Peoples Health Science
Institute and Peoples Health Science Village Centre is one mechanism
which if tried well can prove helpful. This can be done in following:

- Consultation and formation of Peoples Committee in villages/
  Kasabas/ towns/ blocks/Tehsil/Districts.
- A collective resolution/decision by the Peoples Committee to build
  Peoples Health Science centre.

- A collective resolution/decision by the Peoples Committees to raise
  resources through voluntary contributions/donations for the purpose
  of Peoples Health Science Centre.
  (vi) PROPOSAL
  It is proposed to raise resources for the purpose of Peoples Health
  Science Institute in following manner:

- Insurance of every Member of a Family : Every member of a
  family can be insured by paying a small monthly amount of Rs. 5/- or
  10/- fee for free health care. A family of 5 members will be required to
  pay @ Rs. 25/- or Rs. 50/- per month in lieu of free health care at
  Peoples Health Science Institute. Since the Institute shall cover the
  population located within a radius of 150 Kilometers, at least 100,000
  families can be insured in the first phase. This will provide a regular
  monthly income of (Rs. 25x1,00,000) = Rs. 25,00,000 or Rs. 50,00,000
  (Rs. 50x1,00,000). All the details of it shall have to be worked out by the
  experts.

- Membership : Peoples can be made members of the Peoples
  Health Science Institute or Peoples Council of Education. In addition to
  the existing membership of Peoples Council of Education, following
  Peoples Council of Education/21
  categories of membership of Peoples Health Science Institute can be
  created.

- Founder Member : Rs. 100/-

- Founder Life Member : Rs. 2,600/-

- Life Member : Rs. 2,000/-

- Annual Member : Rs. 250/-
  All such members will constitute the General House of Peoples
  Health Science Institute/Peoples Village Health Science Centre and a
  collegium can be formed out of the members. The collegiums can
  manage the affairs of Peoples Health Science Institute. Its details too will
  have to be worked out.

- Donations : An appeal can be made to people of all groups to
  donate liberally to the Peoples Health Science Institute. Philanthropists
  and charitable Trusts can be approached for the purpose.

- Grants : Peoples Council of Education can seek one-time as well
  as recurring grants from the Government of India as well as State
  Governments for the Peoples Health Science Institute. Collaborative
  relationship with Rural Health Mission will have to be built. Rural
  Health Mission can play vital role in the development of Peoples Health
  Science Institutes.

  Since poor peoples may find hard to pay in cash, a different
  approach has to be followed. What the poor people can do is: they can
  provide their labour to the Peoples Health Science Institute for a given
  number of days cost of which can be converted into money as their
  contributions. Through this process the poor people can be made
  members and be insured for their health care.

  Side by side the Peoples Council of Education shall also try to raise their income through varieties
  of co-operative production activities. For example, cooperatives of poor
  peoples can be formed for cultivating medicine and herbal plants, Dairy,
  or any other productive activities. (e.g. tailoring, washing, gardening,
  poultry etc)

  A very strong movement in the field of health science exists in
  today’s India. Quite a number of peoples-oriented health
  centres/hospitals also exist all over India. All these efforts can provide
  the founding principles of the proposed Peoples Health Science Institute
  if a proper networking is done.
