+++
title = "Contact"
description = ""
date = "2019-02-28"
aliases = ["contact"]
author = "Hugo Authors"
+++

## All correspondence to be addressed to:

---

**General Secretary**

Peoples Council of Education,

5, Iswar Saran Ashram Campus.

Prayagraj,

Uttar Pradesh, India.

Pincode: 211004

Mobile: [+91 9935023920](tel:+919935023920)

E-mail: pce1995@gmail.com

---

**Convenor**

Dr. N. P. Chaubey

Lok Swasthya Vigyan Sansthan,

Peoples Council of Education,

5, Iswar Saran Ashram Campus,

Prayagraj,

Uttar Pradesh, India

Pincode: 211004

E-mail: pce1995@gmail.com

Mobile: [+91 6389225222](tel:+916389225222)
