+++
title = "About"
description = ""
date = "2019-02-28"
aliases = ["about-us", "about-hugo", "contact"]
author = "Hugo Authors"
+++

Peoples Council of Education (PCE) is a national educational body with
its registered headquarters at Allahabad. It was established on August 15, 1995
and registered under Society Act of 1861 on June 17, 1998.

Today Peoples Council of Education possesses 3.75 acres of land at Allahabad for construction
of its buildings.A portion of Saubhygavati-Prabhakar Singh Peoples Science
Library building and Nursery Section of Flower Valley Peoples School Building
named ‘Shanti Sadan’ have been constructed on it. It is in process of acquiring
land in Sondebhadra District of U.P. for its proposed Lok Vigyan Sansthan and
Peoples University.

{{< figure src="/images/hospital2.png" class="img-fluid" >}}

To discover, develop and disseminate a new democratic, scientific and
creative education system in consonance with the democratic needs, aspirations
and creative urges/potentialities of peoples of India is the sole objective of
Peoples Council of Education.

Birth of Peoples Council of Education is the result of long-drawn isolated
and nation wide scattered innovative and creative experiments by large number
of dedicated and patriotic individuals/groups in the field of eduction. To be
precise, it summates early experiments in British India by great Indians like
Vidya Sagar, Rabindranath Tagore, Nanabhai Bhatt, M.K. Gandhi, Madan
Mohan Malviya, Sir Syed Ahmed Khan, Zakir Hussain and many others and
over six decades of experiments by various groups(e.g. Krishna Murthy
Foundation, Kishore Bharati, Eklayava, HBCSE, Anand Shiksha Niketan, Lok
Bharati Gram Vidyapith, Gujarat Vidyaptih, Gandhi Gram etc) in Democratic
Republic of India. Lok Shala for Lok Shiksha created by Late Nanabhai Bhatt
and Nai Talim-led Gujarat Vidyapith provide major foundational stones to
Peoples Council of Education. Peoples’ ongoing struggles for free and uniform
Common Education System offer inspirations to it. Declining creativity in
science and ever deepening crisis in education provide context to its formation.
Indian State’s declaration of its withdrawal from education and health provided
impetus to it.

{{< figure src="/images/org2.png" class="img-fluid" >}}

Indian Academy of Social Sciences (ISSA) and its members took major
initiative in conceptualizing and creating Peoples Council of Education.
In a way Peoples Council of Education represents a new initiative by
Peoples of India for creating an altogether a new education system which is an
altogether different from the current British Borne Education System which is
now mixed with the American System of education and, which is integral to
family, community and society. 

Unlike the present education system, the new system ought to be non-alienating, non-violent, non-commercial and harmonizing. The present dual system ought to be replaced by a single common
and uniform system. All forms of racial, caste, class, creed and wealth
discrimination prevalent in the present system have to disappear.
Flowering of creative potentialities/urges of all Indian children through
democratic and scientific education system in Indian languages is the motto of
Peoples Council of Education.

{{< figure src="/images/org1.jpg" class="img-fluid" >}}

Peoples Council of Education is of the people, by the people and for the
people. It seeks active cooperation and support from all Indians, men and
women, rich and poor and educated and uneducated in creating and
disseminating a new democratic and scientific education system for their
wellbeing.
Peoples Council of Education seeks to achieve its objective through the following instruments:

1. Peoples Education Congress (PEC)
2. All India Peoples Medical and Health Science Convention
3. All India Peoples Teachers Convention
4. All India Peoples University and College Teachers Convention
5. [Flower Valley Peoples School](/flower-valley-school/)
6. Lok Vigyan Sanchar Sansthan
7. [Lok Swasthya Vigyan Sansthan](/peoples-health-science-institute/)
8. Lok Praudyogiki Sansthan
9. Peoples University
10. Peoples Science Library and Children’s Library
11. Peoples Education Journals, Monographs And Books in Indian Languages
12. Prabhat Mandal Open Theatre
13. Nanabhai Bhatt Memorial Foundation

Peoples Council of Education is a membership based national educational
organization. All its members constitute its General House which is the supreme
body. General House creates Executive Council for managing the affairs of
Peoples Council of Education. All officials and members of the Executive
Council are elected by the General House through postal ballot for a period of
three years. The Executive Council appoints/nominates all other official/heads
of all the units of Peoples Council of Education.
