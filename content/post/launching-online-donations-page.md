+++
title = 'Launching Our Online Donations Page'
date = 2024-04-29T14:40:27+05:30
draft = false
+++

For the convenience of our donors, We have launched an online donation page by which you could
donate easily to our organisation. There are many payment options available, Such as Credit/Debit Cards, UPI, Netbanking & Mobile wallets.

Please visit our [donate](/donate) page to know more ...

Thank you for your support!
