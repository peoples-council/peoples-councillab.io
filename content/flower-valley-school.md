+++
title = "FLOWER VALLEY PEOPLES SCHOOL"
description = ""
date = "2019-02-28"
aliases = ["flower-valley-school"]
author = "Hugo Authors"
+++

Peoples Council of Education (PCE) established Flower Valley
Peoples School in 1995 as an experiment in the field of education. It had
to be closed in 2005 due to lack of space and has been revived in 2011
after completion of a portion of its building at Meerakpur Uparhar,
Allahabad.


The Flower Valley Peoples School is to provide feedback to
Peoples Council of Education on issues related to new democratic and
scientific education system being developed by it. It is to be different
from the existing schools in its conception, approach, curriculum,
pedagogy and relationships with family, community and society. Its
philosophy and epistemology is rooted in the principles of equality,
freedom and fraternity. Building spirit of cooperation and sharing is its
soul. It does not subscribe to the principles of marketization of education
and duality in education. Nor does it subscribe to the principle of
alienation through education. Rather it seeks to promote friendship and
sense of belongingness with Nature, Family, Community and Society
among its pupils.

{{< figure src="/images/school6.png" class="img-fluid" >}}

It strives to create all the necessary and congenial
conditions for the flowering of creative potentialities of children through
dissemination of holistic and integrated science of Nature-Humans-
Society. Stimulating curiosity, creative thinking and exploratory actions
is central to the philosophy of Flower Valley Peoples School. Since this
calls for education in one’s own mother tongue, the Flower Valley
Peoples School places premium on education in Indian languages.

The problem of difference between home language and school language
receives due attention at Flower Valley Peoples School. While learning
in their own language, the students are encouraged to learn other Indian
languages as well as English and other foreign languages.
Valuation of unity of physical and mental labour in education is
very high in the scheme of Flower Valley Peoples School.
Flower Valley Peoples School strives to build closer bonds with
family, community and society through frequent interactions, dialogues,
exchanges. In fact, it is trying to develop an educational progrmames for
parents, community and society with a hope that the Peoples shall learn
Peoples Council of Education to own and manage the Flower Valley School soon.

{{< figure src="/images/school1.png" class="img-fluid " >}}

It is hoped that the richer sections of community and society shall bear the cost of education
of the children from weaker sections of community and society. State too
should come forward to support it liberally.
Peoples Council of Education proposes to take care of education of
orphan, handicapped and extremely poor children. It will build hostels
and all the necessary homelike care facilities for such children. This,
however, is possible only through the support by society, state and
philanthropists.


Well-meaning individuals/groups/trusts/foundations are
welcome to donate liberally. One can adopt a child from economically
weaker family for his/her education from Nursery to University. One can
also give one time endowment for scholarship, gold medals and
memorial lectures.
Flower Valley Peoples School is located in 3.75 acres land at the
bank of the mighty river Yamuna. Its campus will have following
buildings:

- Nursery/Pre-Primary School
- Primary School
- High School
- Intermediate
- Peoples Teacher Training Institute
- Children’s Library
- Peoples Library
- P.K. Mandal Open Theatre
- Lok Vigyan Sanchar Sansthan
- Hostel
- Guest House

{{< figure src="/images/school4.png" class="img-fluid" >}}

At the moment, a portion of Nursery/Pre-Primary School Building
named ‘Shanti Sadan’ has been constructed. Inspiration Groups of
Kochi based architects have prepared the design of buildings free of
cost. Shanti Sadan has been constructed with the help of donations
received from Prof. Vinod K. Gaur and his brother Sri Prabodh Gaur.
Peoples Council looks forward for generous donations/contributions for
completing construction of the buildings.

At the moment there are 76 children and 6 teachers in Flower Valley
School. Supporting staff includes, one peon, one maid, one driver and
one sweeper. Dr. Sushma, the Principal of the Flower Valley Schools
heads it without any remuneration. She has been paying for the
remuneration of the driver out of her own resources.
The Executive Council of Peoples Council of Education, which
manages it, has set up following committees:

- Managing Committee under chairpersonship of Prof. Pratima Gaur.
- Children’s Library Committee under the chairmanship of Air Vice-
  Marshal Vishwa Mohan Tiwari. Dr. Keshave Prasad Sinha is its convener.

It is in process of setting up following committees:

- Curriculum Development Committee
- Text Books/Story Books Production Committee
- Teachers’ Training Committee

{{< figure src="/images/school2.png" class="img-fluid " >}}

It is also planning establishment of Peoples Nursery School, Teachers Training Institute and Peoples Teachers Training Institute
Success in this experiment will lead to establishment of hundreds of
Flower Valley Peoples School all over India

{{< figure src="/images/school5.png" class="img-fluid " >}}

Peoples Council of Education is planning for establishment of
Peoples University. Flower Valley Peoples School will be under the
Peoples University. It is hoped that students of Flower Valley School
shall flower in due course of time.

{{< figure src="/images/school0.png" class="img-fluid" >}}

---

# DONATIONS

Donations meant for Flower Valley School are most welcome from
all those who wish to change the present education system for the
betterment of the people of India. All donations are exempt from Income
Tax under 80 (G) of Income Tax.

Donations are accepted online via our [donations page](/donate) or can be sent by account payee DD/Cheque in favour of Peoples Council of Education payable at Allahabad to the following under registered post:

Dr. N.P. Chaubey

General Secretary

Peoples Council of Education

Iswar Saran Ashram Campus

Allahabad 211 004 (U.P.)

Email: pce1995@gmail. com

Tel: 0532-2684399

It is necessary for the donor to mention the purpose for which the
donation is being made in his/her letter. Donors would be most welcome
to visit the site and hold discussions with the Peoples Council of
Education on mutually convenient dates.
All accounts of donations are open for cross check and verification
by the respective donors.

Donations are most welcome for any or all of the following:

- Buildings
- Children’s Library
- Annual Educational Cost of one or two children
- Endowment for scholarship, gold medals and memorial lectures
- Books, stationeries, dresses and toys, etc for needy children.
- Cost of Midday Meals for specified number of needy children
- Games materials
- Vehicles
- Computer and Internet System
- Any other

---

# AN APPEAL

Flower Valley Peoples School needs brilliant, visionary and
dedicated young teachers. Those who wish to create a new democratic
and scientific education system through their devoted and dedicated
pursuits as teachers are most welcome to the Flower Valley Peoples
School
